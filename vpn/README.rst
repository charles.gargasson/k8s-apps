#########
WireGuard
#########


*****
Image
*****

| https://gitlab.com/charles.gargasson/wireguard

**************
Secret Example 
**************

Secret for server private key

.. code-block:: yaml

  apiVersion: v1
  data:
    PRIVKEY: d2lyZWd1YXJkLmNvbS9xdWlja3N0YXJ0Lw==
  kind: Secret
  metadata:
    name: env-priv
    namespace: vpn
  type: Opaque

I'm using a secret for clients public key declaration as well

.. code-block:: yaml

  apiVersion: v1
  kind: Secret
  metadata:
    name: pubkeys
    namespace: vpn
  stringData:
    users: |
      wg set wg0 peer WW91IGNhbiBjb3VudCB0byAxMDI0IHdpdGggMTAgZmluZ2Vycw== allowed-ips 10.0.0.2/32
      wg set wg0 peer UGVuZ3VpbnMgZG8gaGF2ZSBrbmVlcyAhISEhICAgICAgICAgIA== allowed-ips 10.0.0.3/32
