########
k8s-apps
########

| Lab for `CNCF solutions <https://landscape.cncf.io/>`_.

******
SOURCE
******

| ✅ Apps Source = Public Gitlab Group : https://gitlab.com/bakehouse
| ✅ Infra As Code = Public Gitlab Repo : https://gitlab.com/charles.gargasson/k8s-apps
| ✅ Secrets = Private Gitlab Repo 

| 
| 

*****
BUILD
*****

| ✅ Container Builder = Kaniko (https://github.com/GoogleContainerTools/kaniko)
| ✅ Build environment = Private Gitlab Runner instances running on K8S

| 
| 

******
DEPLOY
******

| ✅ Continuous Delivery = ArgoCD (https://argoproj.github.io/cd)
| ✅ Registry = Harbor (https://goharbor.io/)

| 
| 

-----------

***
RUN
***

| ✅ Network = Calico (https://www.tigera.io/project-calico/)
| ✅ Proxy = Istio (https://istio.io/)
| ✅ Service Mesh = Istio (https://istio.io/)
| ✅ Storage = Rook ceph (https://rook.io/)

| 
| 

************************
MONITORING & SUPERVISION
************************

| ✅ Metrics collection = Istio default envoy sidecar
| ✅ Metrics processing = Prometheus integration for Istio (https://prometheus.io/)
| ✅ Visualization = Grafana integration for Istio (https://grafana.com/)

| 
| 

*******
Logging
*******

| ❌ Log collection (k8s default stderr/out logging, looking for fluentbit or promtail configuration...)
| ❌ Log aggregation (Loki maybe ?)
| ❌ Log visualization (grafana)


| 
| 


***********************
SECURITY BEST PRACTICES
***********************

| ✅ Make use of non root users container when possible
| ✅ Blocking network connections between containers by default

| 
| 

*****************
PERFORMANCE & SLA
*****************

| ❌ External IP LB / FailOver

| 
| 
