#####
PyKMS
#####


*****
Image
*****

| https://py-kms.readthedocs.io/en/latest/Documentation.html
| https://github.com/SystemRage/py-kms


****
Tuto
****

| Run in administrator terminal, 
| example for W10 pro version, replace with your windows version key (https://py-kms.readthedocs.io/en/latest/Keys.html)

.. code-block:: bat

    cscript //nologo slmgr.vbs /upk
    cscript //nologo slmgr.vbs /ipk W269N-WFGWX-YVC9B-4J6C9-T83GX
    cscript //nologo slmgr.vbs /skms offensive.run:1688
    cscript //nologo slmgr.vbs /ato
    cscript //nologo slmgr.vbs /ato
    cscript //nologo slmgr.vbs /dlv
