#######
CoreDNS
#######

*****
Image
*****

| https://gitlab.com/charles.gargasson/coredns

**************
Secret Example
**************

.. code-block:: yaml

  apiVersion: v1
  kind: Secret
  metadata:
    name: confdns
    namespace: default
  stringData:
    Corefile: |
      gargasson.com {
        reload 10s
        minimal
        hosts {
          1.2.3.4 x.gargasson.com
          fallthrough
        }
        whoami
        log
      }
