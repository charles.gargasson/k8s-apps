from graphviz import render
import os

dir = os.path.dirname(os.path.realpath(__file__))
path = f"{dir}/test.gv"
render('dot', 'png', path)

